import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cart: [],

    StoreCarts: [],
    allProducts: [],

  },
  getters: {
    allProducts: (state) => state.allProducts,
  },
  mutations: {
    addToCart(state, data){
      state.cart = [...state.cart, data]
    },
    ADD_Item(state, id) {
      state.StoreCarts.push(id);
    },
    REMOVE_Item(state, id) {
      state.StoreCarts.splice(state.StoreCarts.indexOf(id),1);
    },
    REMOVE_Food_Item(state, id) {
      state.allProducts.splice(state.StoreCarts.indexOf(id),-1);
      localStorage.setItem('items', JSON.stringify(state.allProducts));

    },
    SET_PRODUCT(state,data) {
      let {index,item} = data
      state.StoreCarts[index] = item;
    },
    addProducts(state, item){
      state.allProducts = item;
    },
    updateCart(state, item) {
      state.allProducts = [...state.allProducts, item]
    },
    resetState(state){
state.allProducts= []
    }
  },
  actions: {
    addItem(context, id) {
      context.commit("ADD_Item", id);
    },

    removeItem(context, index) {
      context.commit("REMOVE_Item", index);
    },
    removeFoodItem(context, index) {
      context.commit("REMOVE_Food_Item", index);
    },
  },
  modules: {
  },
  plugins: [createPersistedState()],
})
