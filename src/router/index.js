import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'login',
    component: () => import('../views/Login'),
    meta:{
      title:'Sign In'
    }
  },
  {
    path: '/signup',
    name: 'SignIn',
    component: () => import('../views/SignUp.vue'),
    meta: {
      title: 'Sign Up'
    }
  },
  {
    path:'/main_page',
    name: 'Main Page',
    component: () => import('../views/Main.vue'),
    meta:{
      title: 'Main Page'
    }
  },
  {
    path:'/add',
    name:'Add New Items',
    component:() => import('../views/AddNewItems'),
    meta:{
      title: 'Add New Items'
    }
  },
  {
    path:'/cart',
    name:'Cart',
    component:() => import('../views/Cart'),
    meta:{
      title: 'Items in Cart'
    }
  },
  {
    path:'/deleteItem',
    name:'DeleteItems',
    component:() => import('../views/DeleteItems'),
    meta:{
      title: 'Delete Items'
    }
  }
]



const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})





  // const checkToken = () => {
  //   if(localStorage.getItem('token') == (null || undefined) ){
  //     console.log('token is not there : ' + localStorage.getItem('token'));
  //     return false;
  //   }
  //   else{
  //     return true
  //   }
  // }




router.beforeEach((to, from, next) => {
  if (localStorage.getItem('token') === null && to.path !== "/") {
    if(to.path === "/signup"){
      next();
    }else{
      next("/");
    }
  } else {
    if (localStorage.getItem('token')
        !== null && to.path === "/") {
      next('main_page');
    }else{
      next();
    }




  }
});

export default router
